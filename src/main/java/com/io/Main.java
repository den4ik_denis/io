package com.io;

import com.io.services.FileServices;
import com.io.wrappers.BufferedReaderWrapper;
import com.io.wrappers.FileWriterWrapper;

public class Main {

    public static void main(String[] args) {
        FileServices fileServices = new FileServices(new BufferedReaderWrapper(), new FileWriterWrapper());
        String[] fileNames = {"DataStore\\old_file_1.txt",
        "DataStore\\old_file_2.txt",
        "DataStore\\old_file_3.txt"};
        //System.out.println(fileServices.readFiles(fileNames));

//        String str1 = "hhhhhhhhhh\nhhhhhhhhhhhhhhhhhhhhhh\nhhhhhhhhhhhhhhhhhhh";
//        String str2 = "jjjjjjjjjjj\njjjjjjjjjjjjjjjjjjjjjjj\njjjjjjjjjjjjjjjjj";
//        String str3 = "yyyyyyyyyyy\nyyyyyyyyyyyyyyyyyyyyyyyy\nyyyyyyyyyyyyyyyy";
//
//        fileServices.writeFileWithDelete(str1, "DataStore\\new_file_1.txt");
//        fileServices.writeFileToStart(str2, "DataStore\\new_file_1.txt");
//        fileServices.writeFileToEnd(str3, "DataStore\\new_file_1.txt");

        String name = "DataStore/new_file_2.txt";

        String[] n = {name};

        fileServices.readFiles(n);
    }
}
