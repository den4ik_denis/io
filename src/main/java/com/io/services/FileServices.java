package com.io.services;

import com.io.wrappers.BufferedReaderWrapper;
import com.io.wrappers.FileWriterWrapper;

import java.io.*;
import java.util.ArrayList;

public class FileServices {
//    Задание.
//    Выполнить вычитку текста из файлов с заданными именами (количество файлов >= 1 и <= Byte.Max).
//    Записать считанные данные в другой файл.  Должно быть предусмотрено 3 режима записи:
//    Стереть все существующие данные и записать новые данные. +
//    Записать данные в начало файла. Существующие данные не затираются. +
//    Записать данные в конец файла. Существующие данные не затираются. +

    private FileWriterWrapper writerWrapper;
    private BufferedReaderWrapper readerWrapper;

    private static final String EMPTY_STRING_FOR_ADD = "";

    public FileServices(BufferedReaderWrapper readerWrapper, FileWriterWrapper writerWrapper) {
        this.readerWrapper = readerWrapper;
        this.writerWrapper = writerWrapper;
    }

    public String readFiles(String[] fileNames) {
        if(fileNames == null){
            return null;
        }
        int arrayLength = fileNames.length;
        if (arrayLength < 1 || arrayLength > Byte.MAX_VALUE) {
            return null;
        }
        ArrayList<File> files = new ArrayList<>();
        for (int i = 0; i < fileNames.length; i++) {
            if (fileNames[i] != null) {
                files.add(new File(fileNames[i]));
            }
        }
        String allText = EMPTY_STRING_FOR_ADD;
        for (File file : files) {
            String fileText = readFile(file);
            if (fileText != null&&allText.length() !=0){
                fileText = "\n"+fileText;
            }
            allText += fileText;
        }
        return allText;
    }

    private String readFile(File file) {
        String fileText = EMPTY_STRING_FOR_ADD;

        try(BufferedReader reader = readerWrapper.getBufferedReader(file)) {
            String fileString = reader.readLine();

            while (fileString != null) {
                fileText += fileString;
                fileText += "\n";
                fileString = reader.readLine();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        if (fileText.length()>0){
        fileText = fileText.substring(0, fileText.length() - 1);
        }

        return fileText;
    }

    public void writeFileWithDelete(String text, String fileName) {
        if (fileName == null&&text == null) {
            return;
        }
        File file = new File(fileName);
        try (FileWriter writer = writerWrapper.getFileWriter(file, false)) {
            writer.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void writeFileToEnd(String text, String fileName) {
        if (fileName == null||text == null){
            return;
        }
        File file = new File(fileName);
        if (file.length() != 0) {
            text = "\n" + text;
        }
        try (FileWriter writer = writerWrapper.getFileWriter(file, true)) {
            writer.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public  void writeFileToStart(String text, String fileName) {
        if (fileName == null||text == null){
            return;
        }
        File file = new File(fileName);
        String fileText = readFile(file);
        if (fileText.length() > 0){
            text += "\n"+fileText;
        }
        try (FileWriter writer = writerWrapper.getFileWriter(file, false)) {
            writer.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
